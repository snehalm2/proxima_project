package main

import (
	"github.com/snehal-mohite/Projects/API/Controllers"
	_ "github.com/snehal-mohite/Projects/API/Response"
	_ "github.com/snehal-mohite/Projects/API/model"
)

func main() {
	Controllers.Run()

}

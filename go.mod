module github.com/snehal-mohite/Projects

go 1.16

require (
	github.com/gorilla/mux v1.8.0
	github.com/jinzhu/gorm v1.9.16
	github.com/joho/godotenv v1.3.0
	golang.org/x/crypto v0.0.0-20210322153248-0c34fe9e7dc2 // indirect
)

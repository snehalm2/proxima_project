package Controllers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/snehal-mohite/Projects/API/Response"
	"github.com/snehal-mohite/Projects/API/model"
)

func (server *Server) CreateProject(w http.ResponseWriter, r *http.Request) {

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		Response.ERROR(w, http.StatusBadRequest, err)
		return
	}
	Project := model.Projects{}
	err = json.Unmarshal(body, &Project)
	if err != nil {
		Response.ERROR(w, http.StatusBadRequest, err)
		return
	}
	Project.Prepare()
	err = Project.Validate("")
	if err != nil {
		Response.ERROR(w, http.StatusBadRequest, err)
		return
	}
	ProjectCreated, err := Project.SaveProject(server.DB)

	if err != nil {
		Response.ERROR(w, http.StatusBadRequest, err)
		return
	}
	w.WriteHeader(http.StatusCreated)
	w.Header().Set("Location", fmt.Sprintf("%s%s/%d", r.Host, r.RequestURI, ProjectCreated.ID))
	fmt.Fprintf(w, "Project Created successfully")

}

func (server *Server) Getprojects(w http.ResponseWriter, r *http.Request) {

	Project := model.Projects{}

	Projects, err := Project.FindAllProjects(server.DB)
	if err != nil {
		Response.ERROR(w, http.StatusBadRequest, err)
		return
	}
	Response.JSON(w, http.StatusOK, Projects)
}

func (server *Server) GetProject(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	pid, err := strconv.ParseUint(vars["id"], 10, 32)
	if err != nil {
		Response.ERROR(w, http.StatusBadRequest, err)
		return
	}
	Project := model.Projects{}
	ProjectGot, err := Project.FindProjectByID(server.DB, uint32(pid))
	if err != nil {
		Response.ERROR(w, http.StatusBadRequest, err)
		return
	}
	Response.JSON(w, http.StatusOK, ProjectGot)
}

func (server *Server) UpdateProject(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	pid, err := strconv.ParseUint(vars["id"], 10, 32)
	if err != nil {
		Response.ERROR(w, http.StatusBadRequest, err)
		return
	}
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		Response.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	Project := model.Projects{}
	err = json.Unmarshal(body, &Project)
	if err != nil {
		Response.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	Project.Prepare()
	err = Project.Validate("update")
	if err != nil {
		Response.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	updatedProject, err := Project.UpdateProject(server.DB, uint32(pid))
	if err != nil {

		Response.ERROR(w, http.StatusBadRequest, err)
		return
	}
	w.Header().Set("Location", fmt.Sprintf("%s%s/%d", r.Host, r.RequestURI, updatedProject.ID))
	fmt.Fprintf(w, "Project updated successfully")

}

func (server *Server) DeleteProject(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)

	Project := model.Projects{}

	pid, err := strconv.ParseUint(vars["id"], 10, 32)
	if err != nil {
		Response.ERROR(w, http.StatusBadRequest, err)
		return
	}
	_, err = Project.DeleteProject(server.DB, uint32(pid))
	if err != nil {
		Response.ERROR(w, http.StatusBadRequest, err)
		return
	}
	w.Header().Set("Entity", fmt.Sprintf("%d", pid))
	fmt.Fprintf(w, "Project deleted successfully")

}

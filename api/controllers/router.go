package Controllers

import (
	"net/http"
)

func SetMiddlewareJSON(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		next(w, r)
	}
}

func (s *Server) initializeRoutes() {

	// Home Route

	//Users routes
	s.Router.HandleFunc("/Projects", SetMiddlewareJSON(s.Getprojects)).Methods("GET")
	s.Router.HandleFunc("/Projects/{id}", SetMiddlewareJSON(s.GetProject)).Methods("GET")
	s.Router.HandleFunc("/Project", SetMiddlewareJSON(s.CreateProject)).Methods("POST")
	s.Router.HandleFunc("/Project/{id}", SetMiddlewareJSON(s.UpdateProject)).Methods("PUT")
	s.Router.HandleFunc("/Project/{id}", SetMiddlewareJSON(s.DeleteProject)).Methods("DELETE")
}

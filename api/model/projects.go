package model

import (
	"errors"
	"html"
	"strings"
	"time"

	"github.com/jinzhu/gorm"
)

type Projects struct {
	ID           uint32    `gorm:"primary_key;unique;not null,auto_increment" json:"projectId"`
	ProjectName  string    `gorm:"not null;unique" json:"projectName"`
	CustomerName string    `gorm:"not null" json:"customerName"`
	TechStack    string    `gorm:"not null" json:"techStack"`
	Domain       string    `gorm:"not null;" json:"domain"`
	KeyPerson    string    `gorm:"not null;" json:"keyPerson"`
	ShortDesc    string    `gorm:"not null;" json:"shortDescription"`
	Details      string    `gorm:"not null;" json:"details"`
	CreatedAt    time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt    time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}

func (p *Projects) Prepare() {
	p.ProjectName = html.EscapeString(strings.TrimSpace(p.ProjectName))
	p.CustomerName = html.EscapeString(strings.TrimSpace(p.CustomerName))
	p.TechStack = html.EscapeString(strings.TrimSpace(p.TechStack))
	p.Domain = html.EscapeString(strings.TrimSpace(p.Domain))
	p.KeyPerson = html.EscapeString(strings.TrimSpace(p.KeyPerson))
	p.ShortDesc = html.EscapeString(strings.TrimSpace(p.ShortDesc))
	p.Details = html.EscapeString(strings.TrimSpace(p.Details))
	p.CreatedAt = time.Now()
	p.UpdatedAt = time.Now()
}

func (p *Projects) Validate(action string) error {
	switch strings.ToLower(action) {
	case "update":
		if p.ProjectName == "" {
			return errors.New("Required Project Name")
		}
		if p.CustomerName == "" {
			return errors.New("Required customer Name")
		}
		if p.TechStack == "" {
			return errors.New("Required Technology Stack")
		}
		if p.Domain == "" {
			return errors.New("Required Domain")
		}
		if p.KeyPerson == "" {
			return errors.New("Required Key Person")
		}
		if p.ShortDesc == "" {
			return errors.New("Required Short Description")
		}
		if p.Details == "" {
			return errors.New("Required Project Details")
		}

		return nil
	/*case "login":
	if p.Name == "" {
		return errors.New("Required Project Name")
	}
	if p.KeyPerson == "" {
		return errors.New("Required Key Person")
	}

	return nil*/

	default:
		if p.ProjectName == "" {
			return errors.New("Required Project Name")
		}
		if p.CustomerName == "" {
			return errors.New("Required customer Name")
		}
		if p.TechStack == "" {
			return errors.New("Required Technology Stack")
		}
		if p.Domain == "" {
			return errors.New("Required Domain")
		}
		if p.KeyPerson == "" {
			return errors.New("Required Key Person")
		}
		if p.ShortDesc == "" {

			return errors.New("Required Short Description")
		}
		if p.Details == "" {
			return errors.New("Required Project Details")
		}

		return nil
	}
}

func (p *Projects) SaveProject(db *gorm.DB) (*Projects, error) {

	err := db.Debug().Create(&p).Error
	if err != nil {
		return &Projects{}, err
	}
	return p, nil
}

func (p *Projects) FindAllProjects(db *gorm.DB) (*[]Projects, error) {
	var err error
	Project := []Projects{}
	err = db.Debug().Model(&Projects{}).Limit(100).Find(&Project).Error
	if err != nil {
		return &[]Projects{}, err
	}
	return &Project, err
}

func (p *Projects) FindProjectByID(db *gorm.DB, pid uint32) (*Projects, error) {

	err := db.Debug().Model(Projects{}).Where("id = ?", pid).Take(&p).Error
	if err != nil {
		return &Projects{}, err
	}
	if gorm.IsRecordNotFoundError(err) {
		return &Projects{}, errors.New("Projects Not Found")
	}
	return p, err
}

func (p *Projects) UpdateProject(db *gorm.DB, pid uint32) (*Projects, error) {

	// To hash the project name
	var err error
	db = db.Debug().Model(&Projects{}).Where("id = ?", pid).Take(&Projects{}).UpdateColumns(
		map[string]interface{}{

			"project_name":  p.ProjectName,
			"customer_name": p.CustomerName,
			"tech_stack":    p.TechStack,
			"domain":        p.Domain,
			"key_person":    p.KeyPerson,
			"short_desc":    p.ShortDesc,
			"details":       p.Details,
			"updated_at":    time.Now(),
		},
	)
	if db.Error != nil {
		return &Projects{}, db.Error
	}
	err = db.Debug().Model(&Projects{}).Where("id = ?", pid).Take(&p).Error
	if err != nil {
		return &Projects{}, err
	}
	return p, nil

}

func (p *Projects) DeleteProject(db *gorm.DB, pid uint32) (int64, error) {

	db = db.Debug().Model(&Projects{}).Where("id = ?", pid).Take(&Projects{}).Delete(&Projects{})

	if db.Error != nil {
		return 0, db.Error
	}
	return db.RowsAffected, nil
}
